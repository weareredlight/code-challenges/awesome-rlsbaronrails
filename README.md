# Awesome RlsBarOnRails

Welcome to the technical challenge! Thanks for taking the time to go through our
interview process. We'll try to keep it brief.

The challenge is divided in 3 levels:
- Level 1 should be done by everyone
- Level 2 should be done by candidates to mid-level and above
- Level 3 should be done by candidates to senior positions

Everyone is free to try and tackle later levels for bonus points of course :)


For this challenge we will ask you to create a simple Rails API that should feed
a cocktail recipe app.

We provide you with some base code which includes an early stage of the
`Ingredient` and `Category` models, and will ask you to implement some features
on top of that.


## Development setup
For the development process we'll adopt Rails v6.0.3 (API only) and Ruby version 2.6.5.

### 1) Clone repository

    $ git clone git@gitlab.com:weareredlight/code-challenges/awesome-rlsbaronrails.git

or using HTTPS:

    $ git clone https://gitlab.com/weareredlight/code-challenges/awesome-rlsbaronrails.git

### 2) Install dependencies - Bundle
  Run:

    $ bundle

### 3) Setup database
  Run:

    $ rails db:migrate:reset; rails db:seed

### 4) Start server
  Run:

    $ rails s -p 3000 [-b 0.0.0.0]


## Level 1 - if you are applying for a junior position or above

Your goal is to create the following endpoints on the `CocktailsController`:
1. Display every detail of a certain cocktail. (i.e. Ingredients, Category,
   Instructions, etc) - [show]
2. List all cocktails while allowing searching by cocktail name and by
   ingredient name (partial matches are allowed) - [index with search]
3. On the list of cocktails, show the number of ingredients each cocktail has.

*Bonus Points:*
- Add automated tests for the code you implemented.

*Notes:*
- You should update the Ingredient and Category models and create all the
tables/columns necessary to complete the assignment. Keep in mind that a
cocktail should have many ingredients and only one category.
- We provide you with a list of cocktails in the seeds file. You can use that to
seed the database.
- Make sure you write clean code and use Rails best practices.


## Level 2 - if you are applying for a mid-level position or above

First, please complete any items in the _Bonus Points_ for Level 1.

Your goal is to:
1. On the cocktail list, get results both from the database as well as the
   [Cocktail DB API](https://www.thecocktaildb.com/api.php), sort them by name,
   and render the result.
2. Change the search to match only beginnings of words. For example, searching
   for "o fas" should return a cocktail named "Old Fashioned" (if it exists in
   the database). You can disregard results coming from the Cocktail DB API for
   this.

Tests are mandatory on this level.


## Level 3 - if you are applying for a senior position or above - WIP

1. Allow people to submit new cocktails by sending an email to our app.
2. Please create a `Dockerfile` that would allow running this app in a
   containerized environment. Remember we'll need to pass in some environment
   variables for configuring the DB and such.

*Bonus Points:*
- Add a `docker-compose.yml` that uses the above image + a database image to
  create a fully functioning environment.
